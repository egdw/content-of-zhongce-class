import requests as rq
from bs4 import BeautifulSoup
import pandas as pd

body = rq.request(url="http://www.hzszczx.cn/category/id88121369496796563200.htm", method="GET")
bs = BeautifulSoup(body.text, features="html.parser")
ul = bs.select_one(".article_list_ul")
# print(ul)
titleAndUrl = ul.select("ul li a.article_li_a")
titleDate = ul.select("li span.article_time")
# print(titleAndUrl)
# print(titleDate)

titles = []
urls = []
dates = []
for i in range(len(titleAndUrl)):
    titles.append(titleAndUrl[i].attrs["title"])
    urls.append(titleAndUrl[i].attrs["href"])
    dates.append(titleDate[i].text)
print(titles)
print(urls)
print(dates)
