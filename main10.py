import json

import requests as rq


def getWeather(stationid=""):
    body = rq.get("http://weather.cma.cn/api/weather/view?stationid={}".format(stationid))  # 根据stationid
    # 获取当前的天气，为空表示根据ip地址获取天气
    json_body = json.loads(body.text)  # 解析json数据
    location = json_body.get("data").get("location")  # 获取当前的data下的location字段
    location = location.get("path") + " (" + str(location.get("latitude")) + "," + str(
        location.get("longitude")) + ")"  # 拼接地址和经纬度信息
    now_weather = json_body.get("data").get("now")  # 提取json中data下的now字段里的数据
    # 今天的天气
    now_humidity = now_weather.get("humidity")  # 获取湿度
    pressure = now_weather.get("pressure")  # 获取压力
    temperature = now_weather.get("temperature")  # 获取温度
    windDirection = now_weather.get("windDirection")  # 获取风向
    windSpeed = now_weather.get("windSpeed")  # 获取风速
    print("您要查询的 {} 的现在天气是：湿度：{}, 气压：{}, 气温：{}, 风向：{}, 风速：{}".format(location,
                                                                                          now_humidity, pressure,
                                                                                          temperature,
                                                                                          windDirection, windSpeed))


def search(query):
    body = rq.get("http://weather.cma.cn/api/autocomplete?q={}&limit=10".format(query))
    # print(body.text)
    json_body = json.loads(body.text)
    print("请选择你要查询的城市：")
    for index, location in enumerate(json_body.get("data")):  # 遍历查询到的城市信息
        print(index, location)
    cityIndex = input("请输入你要查询城市的索引:")  # 判断用户输入的城市索引
    city = json_body.get("data")[int(cityIndex)]  # 根据索引取出用户想要查询的城市信息
    # split可以根据分隔符自动切分，返回一个数组
    # 例如：50656|北安|Beian|中国
    # 会返回[50656,北安,Beian,中国] 所以我这里取第一个索引返回对于的城市id
    return city.split("|")[0]


print("天气查询系统。BY：恶搞大王")
while True:
    address = input("请输入你要查询的地方:")
    cityId = search(address)  # 获取用户选择城市id
    getWeather(cityId)  # 根据城市id查询当地天气
# getWeather("58444")
