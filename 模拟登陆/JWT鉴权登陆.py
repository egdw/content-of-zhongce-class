import json

import requests as rq

post_json = '''{"username":"admin","password":"admin"}'''

body = rq.post("https://login3.scrape.center/api/login", verify=False,
               data=post_json, headers={"Content-Type": "application/json;charset=UTF-8"})
print(body.text)
# 解析返回的token
token = json.loads(body.text).get("token")
if token is not None:
    # 说明登录成功
    get_token = "jwt {}".format(token)
    body = rq.get("https://login3.scrape.center/api/book/?limit=18&offset=0", verify=False,
                  headers={"Authorization": get_token})
    print(body.text)

# 不使用jwt，就无法访问了！
body = rq.get("https://login3.scrape.center/api/book/?limit=18&offset=0", verify=False)
print(body.text)