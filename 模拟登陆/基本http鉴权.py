import requests as rq

sess = rq.session()  # 获取session对象，保存http的登陆状态
sess.auth = ("admin", "admin")  # 针对http basic auth ，分别代表账号和密码
# https://ssr3.scrape.center/
# 电影数据网站，无反爬，带有 HTTP Basic Authentication，适合用作 HTTP 认证案例，用户名密码均为 admin。
body = sess.get("https://ssr3.scrape.center/", verify=False)
if body.status_code == 200:
    print("登陆成功！")
    print(body.text)
else:
    print(body.status_code, "登陆失败！")
