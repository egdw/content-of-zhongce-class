from crypto.Util.Padding import pad
from binascii import b2a_hex
# pip install pycryptodome
from crypto.Cipher import DES


# 加密函数
def des_encode_hex(key, text, iv=''):
    des = DES.new(key=key.encode('UTF-8'), mode=DES.MODE_CBC,
                  iv=iv.encode('UTF-8'))  # 参数 key:密钥 mode:模式一般是DES.MODE_ECB
    pad_pkcs7 = pad(text.encode('UTF-8'), DES.block_size, style='pkcs7')  # 选择pkcs7补全
    encrypt_aes = des.encrypt(pad_pkcs7)  # 把bytes转成16进制
    return b2a_hex(encrypt_aes).decode('UTF-8')


if __name__ == '__main__':
    secret_key = 'WfJTKO9S4eLkrPz2JKrAnzdb'  # 密钥
    text = '{"pStr":"test"}'  # 加密对象
    iv = "D076D35C"  # 偏移量
    print(des_encode_hex(key=secret_key, text=text, iv=iv))
