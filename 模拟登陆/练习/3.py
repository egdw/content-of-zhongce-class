import json

import requests as rq


body = rq.post("http://demo.jeecms.com/cmsmanager/login",
               data={"identity":"test","desStr":"EwSSctvjsSxmeuCGIByvWA==","captcha":"","sessionId":"",
                     "codeMessage":""})
print(body.text)
json_data = json.loads(body.text)
token = json_data.get("data").get("JEECMS-Auth-Token")

# 将ownsite接口数据打印输出
body = rq.get("https://demo.jeecms.com/cmsmanager/sites/ownsite",
              headers={"JEECMS-Auth-Token":token})
print(body.text)

