import requests as rq
from bs4 import BeautifulSoup
import pandas as pd

body = rq.request(url="http://www.hzszczx.cn/category/id88121369496796563200.htm", method="GET")
bs = BeautifulSoup(body.text, features="html.parser")
titleAndUrl = bs.select("ul.article_list_ul li a.article_li_a")
titleDate = bs.select("ul.article_list_ul span.article_time")
# print(titleAndUrl)
# print(titleDate)

titles = []
urls = []
dates = []
for i in range(len(titleAndUrl)):
    titles.append(titleAndUrl[i].attrs["title"])
    urls.append(titleAndUrl[i].attrs["href"])
    dates.append(titleDate[i].text)
print(titles)
print(urls)
print(dates)
