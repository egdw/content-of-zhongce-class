import requests as rq
import json # 导入json

def search(keyword, curPage, pageSize):
    body = rq.get(
        "https://api.doutub.com/api/bq/search?keyword={}&curPage={}&pageSize={}".format(keyword, curPage, pageSize))
    print(body.text)
    # 返回的内容是json格式的数据，需要利用python进行解析.
    json_body = json.loads(body.text)
    print(json_body.get("code"), json_body.get("msg"))
    for data in json_body.get("data").get("rows"):
        print(data.get("imgDescribe"), data.get("imgName"), data.get("path"))


search("委屈", 1, 40)
