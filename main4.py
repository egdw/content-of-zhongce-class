import requests as rq
from bs4 import BeautifulSoup
import time

body = rq.get("https://m.hcring.com/details/280683")
bs = BeautifulSoup(body.text, features="html.parser")
down_items = bs.select(".down-item")
for item in down_items:
    musicName = item.select(".ptitle a")[0].text
    musicInfo = item.select(".ringmessage")[0].text
    downloadfile = item.select(".controlRing .startDown")[0].attrs['data-href']
    print(musicName, musicInfo, downloadfile)
    with open("music/"+musicName,"wb") as f:
        f.write(rq.get(downloadfile).content)
