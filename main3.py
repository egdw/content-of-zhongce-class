import requests as rq
from bs4 import BeautifulSoup
import time

body = rq.get("https://www.diydoutu.com/diy/biaoqing?page=1")
bs = BeautifulSoup(body.text, features="html.parser")
imgs = bs.select(".col a img")
for img in imgs:
    time.sleep(1)
    print(img.attrs['alt'], img.attrs['data-src'])
    with open("img/" + img.attrs['alt'] + ".gif", "wb") as f:
        f.write(rq.get(img.attrs['data-src']).content)
