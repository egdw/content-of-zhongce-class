from flask import Flask
from bs4 import BeautifulSoup
import requests as rq
app = Flask(__name__)

# 爬取数据，给前端提供数据
def scrapy(page):
    rq_page=None
    if page == 1:
        rq_page = "tzgg.htm"
    else:
        rq_page = "tzgg/{}.htm".format(43-page)
    body = rq.get("https://www.tourzj.edu.cn/lyxw/{}".format(rq_page))
    body.encoding = body.apparent_encoding
    bs = BeautifulSoup(body.text,features="html.parser")
    news_list = bs.select("div.list_content ul li")
    title_list = []
    date_list = []
    for news in news_list:
        spans = news.select("span")
        title = spans[0].text.strip()
        date = spans[1].text.strip()
        title_list.append(title)
        date_list.append(date)
    return title_list, date_list

@app.route("/page/<page>")
def hello_world(page):
    print(page)
    title_list, date_list = scrapy(int(page))
    return_str = "<ul>"
    for title,date in zip(title_list,date_list):
        return_str = return_str + "<li><span>{}</span><span>{}</span></li>".format(title,date)
    return_str = return_str + "</ul>"
    return return_str


app.run(debug=True)