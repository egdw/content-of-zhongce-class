from flask import Flask, render_template
import pymysql as mysql

app = Flask(__name__)
conn = mysql.connect(host="127.0.0.1", user="root",
                     password="111111", database="world")
cursor = conn.cursor()

# 实现通过网页传入表名查询表结果
@app.route("/<databases>")
def index(databases="city"):
    cursor.execute("select * from {}".format(databases))  # 搜索所有的表数据
    data = cursor.fetchall()
    return render_template("index.html", datas=data)  # 将数据传给前端

if __name__:
    app.run(debug=False)
    cursor.close()
