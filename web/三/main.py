import json

from flask import Flask
from flask import render_template
from bs4 import BeautifulSoup
import requests as rq
app = Flask(__name__)

# 爬取数据，给前端提供数据
def scrapy(page):
    rq_page=None
    if page == 1:
        rq_page = "tzgg.htm"
    else:
        rq_page = "tzgg/{}.htm".format(43-page)
    body = rq.get("https://www.tourzj.edu.cn/lyxw/{}".format(rq_page))
    body.encoding = body.apparent_encoding
    bs = BeautifulSoup(body.text,features="html.parser")
    news_list = bs.select("div.list_content ul li")
    datas = []
    for news in news_list:
        spans = news.select("span")
        title = spans[0].text.strip()
        date = spans[1].text.strip()
        datas.append({"title":title,"date":date})
    return datas


@app.route("/page/<page>")
def pages(page):
    print(page)
    datas = scrapy(int(page))
    print(json.dumps(datas))
    return json.dumps(datas)

@app.route("/")
def hello_world():
    return render_template("index.html")


app.run(debug=True)