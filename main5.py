import os

import requests as rq
from bs4 import BeautifulSoup

body = rq.get("http://www.giabbs.com/topic/31101/i-62698/")
bs = BeautifulSoup(body.text, features="html.parser")
articles = bs.select("article")
for article in articles:
    uid = article.select(".u-img img")[0].attrs["alt"]
    time = article.select(".u-time-format")[0].attrs["utc-time"]
    text = article.select("a.l-item-link")[0].text
    dir_path = "cutecat/" + uid
    if not os.path.exists(dir_path):
        os.mkdir(dir_path)
    with open(dir_path + "/comment.txt", "wt") as f:
        f.write(text)
    for i, img in enumerate(article.select("a.l-img-link span.u-img img")):
        data_original = img.attrs.get("data-original")
        if data_original is not None:
            with open(dir_path + "/{}.jpg".format(i), "wb") as f:
                f.write(rq.get(data_original).content)
    print(uid, time, text)
