# 杭州中策 爬虫上课内容

> 该仓库主要存放上课的代码， 方便同学们自行复习。


## main.py

主要涉及http://www.hzszczx.cn/category/id88121369496796563200.htm网站的文章标题、发表时间和网页链接的抓取。

## main2.py

主要涉及http://www.hzszczx.cn/category/id88121369496796563200.htm网站的文章标题、发表时间和网页链接的抓取的另外一种形式。

## main3.py

主要https://www.diydoutu.com/diy/biaoqing?page=1将所有的图片保存到本地

## main4.py

下载所有的音乐

## main5.py

将该网页内的所有评论内容和图片进行下载,详细结构请看cutecat目录,按照我cutecat目录的标准实现.

## main6.py

原链接已经失效,可以尝试这个网站 http://www.nkjjj.cn/company/567/

要求将所有7页的所有内容进行爬取,包括标题、地点、公司、主营业务等。

## main7.py

利用pymysql将所有抓取的学校列的信息存入mysql数据库中.

## main8.py

https://spa3.scrape.center 数据抓取,注意这个网站是基于ajax的请求,即不是直接用html.

## main9.py

如果利用python输入指定的关键词和页数等,实现https://www.doutub.com 上同等的表情包搜索呢?

## main10.py

利用http://weather.cma.cn查找全国任意城市的天气,要求输入中文就可以直接获取天气,详情请看下我代码.


## 练习答案

其他练习题
参考:https://gitee.com/egdw/content-of-zhongce-class/raw/master/%E7%BB%83%E4%B9%A0%E7%AD%94%E6%A1%88/%E7%BB%83%E4%B9%A0.pdf

## 模拟登录

各种不同的模拟登录方法. 包括几种常见的鉴权方法,访问几个链接的主网站,看看他们是如何登录的?

## web

基于flask的各种应用.具体看我代码实现,到这里了你应该很多都知道了.
