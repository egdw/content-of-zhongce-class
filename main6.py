import os

import requests as rq
from bs4 import BeautifulSoup
import pandas as pd
titles = []
descs = []
dates = []
statuss = []
locs = []

for i in range(1,4):
    body = rq.get("http://www.nkjjj.cn/invest/486/investlist{}.html".format(i))
    body.encoding = body.apparent_encoding
    bs = BeautifulSoup(body.text, features="html.parser")
    articles = bs.select(".list")
    # for page in ("investlist1.html","investlist2.html","investlist3.html")
    for article in articles:
        # print(article)
        title = article.select("strong.px14")[0].text
        desc = article.select("li.f_gray")[0].text.strip()
        date = article.select("span.f_r")[0].text
        status = article.select("span.f_gray")[0].text
        loc = article.select("td.f_gray")[0].text
        titles.append(title)
        descs.append(desc)
        dates.append(date)
        statuss.append(status)
        locs.append(loc)
        print(title, desc, date, status, loc)

pd = pd.DataFrame({"title": titles, "desc": descs, "date": dates, "location": loc, "status": statuss})
pd.to_csv("家具.csv",encoding="utf_8_sig")
