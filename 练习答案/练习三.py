import json
import os.path

import requests as rq
for i in range(0,10):
    body = rq.get("https://spa4.scrape.center/api/news/?limit=10&offset={}".format(i*10), verify=False)
    json_body = json.loads(body.text)
    results = json_body.get("results")
    for result in results:
        title = result.get("title")
        published_at = result.get("published_at")
        thumb = result.get("thumb")
        print(title,published_at,thumb)
        if not os.path.exists("news/{}".format(title)):  # 判断文件夹是否存在
            os.mkdir("news/{}".format(title))  # 不存在则创建
            if thumb is not None:
                with open("news/{}/{}.jpg".format(title,published_at),"wb") as f:
                    f.write(rq.get(thumb,verify=False).content)
