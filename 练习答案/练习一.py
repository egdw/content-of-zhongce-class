import requests as rq
from bs4 import BeautifulSoup
import pandas as pd

names = []
catess = []
movie_locations=[]
movie_durations=[]
movie_durations=[]
start_play_times=[]
scores=[]
star_nums_list=[]

for i in range(1,11):
    body = rq.get("https://ssr1.scrape.center/page/{}".format(i), verify=False)
    bs = BeautifulSoup(body.text,features="html.parser")
    cards = bs.select("div.el-card__body")
    for card in cards:
        name = card.select_one("h2.m-b-sm").text
        names.append(name)
        categories = card.select("button.category span")

        cates = []  # 临时存放类别
        for category in categories:
            cates.append(category.text)
        catess.append(cates)
        # 防止数组越界，判断是否超出数组长度
        if len(card.select("div.info span")) >= 1:
            movie_location = card.select("div.info span")[0].text  # 获取电影生成地址
        movie_locations.append(movie_location)
        if len(card.select("div.info span")) >= 3:
            movie_duration = card.select("div.info span")[2].text  # 获取电影时长
        movie_durations.append(movie_duration)
        if len(card.select("div.info span")) >= 4:
            start_play_time = card.select("div.info span")[3].text  # 获取电影时长
        start_play_times.append(start_play_time)
        score = card.select_one("p.score").text.strip()  # 评分
        scores.append(score)
        star_nums = len(card.select("i.el-rate__icon"))  # 星级
        star_nums_list.append(star_nums)
        print(name, cates, movie_location,start_play_time, movie_duration, score, star_nums)

# 保存数据到csv中
df = pd.DataFrame(data={"电影名":names,"类别":catess,"电影地址":movie_locations,"上映时间":start_play_times,"电影时长":movie_durations,"电影分数":scores,"星级":star_nums_list})
df.to_csv("movie.csv",encoding="utf-8")